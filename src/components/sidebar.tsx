import {
  Button,
  IconButton,
  Icon,
  Flex,
  Tooltip,
  Divider
} from '@chakra-ui/react'
import { Link } from 'wouter'
import { FaUserCircle, FaEnvelope, FaUser } from 'react-icons/fa'

export const Sidebar = () => (
  <Flex
    flexDir="column"
    justifyContent="start"
    alignItems="start"
    h="full"
    w="full"
    p={8}
    bg="gray.800"
    gap="4">
    <Link to="/">
      <Tooltip label="Profile">
        <IconButton
          aria-label="Profile"
          bg="gray.900"
          color="pink.500"
          boxSize="12"
          borderRadius="md"
          _focus={{
            WebkitTapHighlightColor: 'transparent'
          }}
          icon={<Icon as={FaUserCircle} boxSize="5" />}></IconButton>
      </Tooltip>
    </Link>
    <Link to="/conversations">
      <Tooltip label="Conversations">
        <IconButton
          color="pink.500"
          aria-label="Conversations"
          bg="gray.900"
          boxSize="12"
          borderRadius="md"
          _focus={{
            WebkitTapHighlightColor: 'transparent'
          }}
          icon={<Icon as={FaEnvelope} boxSize="5" />}
        />
      </Tooltip>
    </Link>
  </Flex>
)
