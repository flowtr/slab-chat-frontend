import { Box, Container, HStack, VStack } from '@chakra-ui/react'
import { FC } from 'react'
import { Helmet } from 'react-helmet-async'
import { Sidebar } from './sidebar'

export const Layout: FC = ({ children }) => (
  <Box as="main" minH="100vh" bg="gray.900">
    <Helmet>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <title>Slab Chat</title>
    </Helmet>
    <HStack h="100vh">
      <VStack align="flex-start" h="full">
        <Sidebar />
      </VStack>
      <VStack align="flex-start" h="full" w="100%">
        <Container maxW="container.md" pt={16}>
          {children}
        </Container>
      </VStack>
    </HStack>
  </Box>
)
