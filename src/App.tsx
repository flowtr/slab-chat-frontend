import { ChakraProvider } from '@chakra-ui/react'
import { Route, Router } from 'wouter'
import { HelmetProvider } from 'react-helmet-async'
import { Layout } from './components/layout'
import { theme } from './theme'

export const App = () => (
  <ChakraProvider theme={theme}>
    <HelmetProvider>
      <Layout>
        <Router>
          <Route path="/">
            <h2>Hello world!</h2>
          </Route>
        </Router>
      </Layout>
    </HelmetProvider>
  </ChakraProvider>
)
